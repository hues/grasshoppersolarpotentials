# README #

### General description

Fast Annual Hourly Solar Potential Analysis on meshed geometries.
Based on interpolation of obstruction calculations.
Platform: Rhinoceros 3D / Grasshopper.

![](https://bytebucket.org/hues/grasshoppersolarpotentials/raw/a1238b449751b8d6397aaa7a5fbdbd5a147fed87/solar.gif)

### More information

Copy the file "GrasshopperEnergyTools.gha" into your Grasshopper/Components folder. To find this folder, enter in the Rhino command line: "GrasshopperFolder" and click on "Components".

Open the file "160425_TutorialSolarTool.gh" in Grasshopper as an example.

Forthcomming conference paper: 

Christoph Waibel, Ralph Evins, Jan Carmeliet (2016). Using Interpolation to Generate Hourly Annual Solar Potential Profiles for Complex Geometries. In: 3rd IBPSA England Conference, Newcastle, 12th-14th Sept.2016.

### Authors

Christoph Waibel

### License / Software used

https://www.rhino3d.com/

http://www.grasshopper3d.com/
